package pl.pogo.libbio.entity;

public class BookDetails {
	public String title;
	public String author;
	public Integer id;
	public Integer category;
	public String ISBN;
	public String publisher;
	public Integer count;
	public Integer avail;
}
