package pl.pogo.libbio.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;

import pl.pogo.libbio.entity.BookDetails;
/*
 * TODO: obsluga wyjatkow, ale czas goni
 */
public class DataService {
	static DataService instance=null;
	static Integer status = 0;
	
	//mapowania
	Map<String,Integer> titleToBook=new ConcurrentHashMap<String,Integer>();
	Map<String,List<Integer>> authorToBooks=new ConcurrentHashMap<String,List<Integer>>();
	Map<Integer,List<Integer>> categoryToBooks=new ConcurrentHashMap<Integer,List<Integer>>();
	Map<String,Integer> nameToCategory=new ConcurrentHashMap<String,Integer>();
	Map<Integer,BookDetails> idToBookDetails=new ConcurrentHashMap<Integer,BookDetails>();
	
	
	public static DataService getInstance() //singleton
	{
		if(instance==null)
		{
			instance=new DataService();
		}
		return instance;
	}
	
	void checkAndUpdate() {
			if (status.equals(0)) {
				update();
			}
	}
	
	synchronized void update()//zaciaganie danych z serva
	{
		HttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
		HttpGet httpGet = new HttpGet();
		URI web = null;
		HttpResponse response = null;
		InputStream inputStream = null;
		try {
			web = new URI("http://mswiecki-001-site1.myasp.net/api/getbooks/?gatunek=&author=&isbn=&title=");
		} catch (URISyntaxException e1) {

		}
		httpGet.setURI(web);

		try {
			response = httpclient.execute(httpGet);
		} catch (ClientProtocolException e) {

		} catch (IOException e) {

		}
		if(response!=null)
		{
		HttpEntity entity = response.getEntity();
		try {
			inputStream = entity.getContent();
		} catch (IllegalStateException e) {

		} catch (IOException e) {

		}
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
		} catch (UnsupportedEncodingException e) {

		}
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    try {
			while ((line = reader.readLine()) != null)
			{
			    sb.append(line + "\n");
			}
		} catch (IOException e) {

		}
	    JSONArray books = null;
		try {
			 books = new JSONArray(sb.toString());
		} catch (JSONException e) {

		}
		for(int i=0;i<books.length();i++)
		{
			try {
				BookDetails details=new BookDetails();
				details.id=(Integer) books.getJSONObject(i).getInt("BookId");
				details.title=books.getJSONObject(i).getString("Title").toString();
				details.author=books.getJSONObject(i).getString("Author").toString();
				details.ISBN=books.getJSONObject(i).getString("ISBN").toString();
				details.publisher=books.getJSONObject(i).getString("Publisher").toString();
				details.avail= (Integer) books.getJSONObject(i).getInt("AvailableCount");
				details.count=(Integer) books.getJSONObject(i).getInt("Count");
				JSONArray categories = (JSONArray) books.getJSONObject(i).get("Categories");

				idToBookDetails.put(details.id, details);
				titleToBook.put(details.title, details.id);

				if(authorToBooks.containsKey(details.author))
				{
					authorToBooks.get(details.author).add(details.id);
				}
				else
				{
					List<Integer> authorBooks=new ArrayList<Integer>();
					authorBooks.add(details.id);
					authorToBooks.put(details.author, authorBooks);
				}

				for(int j=0;j<categories.length();j++)
				{
					Integer key=categories.getJSONObject(j).getInt("CategoryId");
					String name=categories.getJSONObject(j).getString("CategoryName");
					if(categoryToBooks.containsKey(key))
					{
						categoryToBooks.get(key).add(details.id);
					}
					else
					{
						List<Integer> idBooks=new ArrayList<Integer>();
						idBooks.add(details.id);
						categoryToBooks.put(key, idBooks);
					}

					nameToCategory.put(name,key);
				}

				//categoryToBooks


			} catch (JSONException e) {

			}
		}
		status = 1;
		}
		else
		{
			status=0;
		}
	}
	
	//return: Title->Id
	public Map<String,Integer> getAll()
	{
		checkAndUpdate();
		return titleToBook;
	}
	
	//return: Category->Id
	public Map<String,Integer> getCategories()
	{
		checkAndUpdate();
		return nameToCategory;
	}
	
	//return: Title->Id
	public Map<String, Integer> searchByCategory(Integer category)
	{
		checkAndUpdate();
		Map<String,Integer> result = new HashMap<String, Integer>();
		for(Integer bookId : categoryToBooks.get(category))
		{
			result.put(idToBookDetails.get(bookId).title, bookId);
		}
		return result;
	}
	
	//return: Title->Id
	public Map<String, Integer> searchByAuthor(String author)
	{
		checkAndUpdate();
		Map<String,Integer> result = new HashMap<String, Integer>();
		for(String processedAuthor : authorToBooks.keySet())
		{
			if(processedAuthor.startsWith(author))
			{
				for(Integer bookId : authorToBooks.get(processedAuthor))
				{
					result.put(idToBookDetails.get(bookId).title, bookId);
				}
			}
		}
		return result;
	}
	
	//return: Title->Id
	public Map<String, Integer> searchByTitle(String title)
	{
		checkAndUpdate();
		Map<String, Integer> result = new HashMap<String, Integer>();
		for(String processedTitle : titleToBook.keySet())
		{
			if(processedTitle.startsWith(title))
			{
					result.put(processedTitle, titleToBook.get(processedTitle));
			}
		}
		return result;
	}
	
	public Integer titleToInteger(String title)
	{
		checkAndUpdate();
		for(String processedTitle : titleToBook.keySet())
		{
			if(processedTitle.equals(title))
			{
					return (titleToBook.get(processedTitle));
			}
		}
		return null;
	}

	public BookDetails getDetails(Integer id)
	{
		checkAndUpdate();
		return idToBookDetails.get(id);
	}
	
	public Integer categoryToInteger(String category)
	{
		checkAndUpdate();
		for(String processedTitle : nameToCategory.keySet())
		{
			if(processedTitle.equals(category))
			{
					return (nameToCategory.get(processedTitle));
			}
		}
		return null;
	}
	
	public Map<String, Integer> categoryToBooks(Integer id)
	{
		HashMap<String,Integer> result=new HashMap<String,Integer>();

		if(categoryToBooks.containsKey(id))
		{
			List<Integer> booksIds = categoryToBooks.get(id);
			for(Integer bookId : booksIds)
			{
				BookDetails tempBook  = getDetails(bookId);
				result.put(tempBook.title, bookId);
			}
		}

		return result;
	}

}
