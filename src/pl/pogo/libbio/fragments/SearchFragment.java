package pl.pogo.libbio.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import pl.pogo.libbio.R;
import pl.pogo.libbio.entity.BookDetails;
import pl.pogo.libbio.services.DataService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class SearchFragment extends Fragment {
    public static final String ARG_SEARCH_TYPE = "search_type";
    View rootView;
    TextView description;
    EditText searchBar;
    ListView listView;
    View searchLayout;
    View loadingLayout;
    
    DataService dataProvider;

    public SearchFragment() {
        dataProvider=DataService.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	final int i = getArguments().getInt(ARG_SEARCH_TYPE);
    	
        rootView = inflater.inflate(R.layout.search_layout, container, false);
        
        final String type = getResources().getStringArray(R.array.search_types)[i];
        
        description=((TextView) rootView.findViewById(R.id.textView1));
        description.setText("Wyszukiwanie "+type);
        
        searchBar=(EditText)rootView.findViewById(R.id.editText1);
        
        listView=(ListView)rootView.findViewById(R.id.listView1);
        
        loadingLayout=rootView.findViewById(R.id.loading_layout);
        
        searchLayout=rootView.findViewById(R.id.search_layout);
        
        //listView.setOnClickListener(l);
        
        if(i<2)//wyszukiwanie
        {
            listView.setOnItemClickListener(new OnItemClickListener() {

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view,
    					int position, long id) {
    				
    				Integer key=dataProvider.titleToInteger(listView.getItemAtPosition(position).toString());
    				BookDetails details = dataProvider.getDetails(key);
    				DetailsFragment frag = new DetailsFragment();
    				frag.setDetails(details);
    				frag.show(getFragmentManager(), "DIALOG_DETAILS");
    				
    			}
    		});
        	loadingLayout.setVisibility(View.GONE);
        	searchLayout.setVisibility(View.VISIBLE);
        	searchBar.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				updateListView(s.toString(), i);

			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
			}
		});
        }
        else if(i==2)
        {
            listView.setOnItemClickListener(new OnItemClickListener() {
    			@Override
    			public void onItemClick(AdapterView<?> parent, View view,
    					int position, long id) {
    				
    				Integer key=dataProvider.categoryToInteger(listView.getItemAtPosition(position).toString());   				
    				updateListView(key.toString(), 4);
    				
    	            listView.setOnItemClickListener(new OnItemClickListener() {

    	    			@Override
    	    			public void onItemClick(AdapterView<?> parent, View view,
    	    					int position, long id) {
    	    				
    	    				Integer key=dataProvider.titleToInteger(listView.getItemAtPosition(position).toString());
    	    				BookDetails details = dataProvider.getDetails(key);
    	    				DetailsFragment frag = new DetailsFragment();
    	    				frag.setDetails(details);
    	    				frag.show(getFragmentManager(), "DIALOG_DETAILS");
    	    				
    	    			}
    	    		});
    			}
    		});
        	
        	searchBar.setVisibility(View.GONE);
        	updateListView("",i);
        }
        else
        {
            listView.setOnItemClickListener(new OnItemClickListener() {

    			@Override
    			public void onItemClick(AdapterView<?> parent, View view,
    					int position, long id) {
    				
    				Integer key=dataProvider.titleToInteger(listView.getItemAtPosition(position).toString());
    				BookDetails details = dataProvider.getDetails(key);
    				DetailsFragment frag = new DetailsFragment();
    				frag.setDetails(details);
    				frag.show(getFragmentManager(), "DIALOG_DETAILS");
    				
    			}
    		});
        	searchBar.setVisibility(View.GONE);
        	updateListView("",3);
        }
        
        getActivity().setTitle("Wyszukiwanie "+type);
        

        return rootView;
    }
    
    void updateListView(String search, int type)
    {
    	UpdateListViewTaskParams params=new UpdateListViewTaskParams();
    	params.search=search;
    	params.type=type;
		
    	new UpdateListViewTask().execute(params);
    }
    
    private class UpdateListViewTask extends AsyncTask<UpdateListViewTaskParams, Void, ArrayAdapter<String>> {
    	@Override
		protected ArrayAdapter<String> doInBackground(
				UpdateListViewTaskParams... params) {
    		List<String> data=new ArrayList<String>();
    		if(params[0].type==0)
    		{
    			Map<String,Integer> result=dataProvider.searchByAuthor(params[0].search);
    			data = new ArrayList<String>(result.keySet());
    		}
    		else if(params[0].type==1)
    		{
    			Map<String,Integer> result=dataProvider.searchByTitle(params[0].search);
    			data = new ArrayList<String>(result.keySet());
    		}
    		else if(params[0].type==2)
    		{
    			Map<String,Integer> result=dataProvider.getCategories();
    			data = new ArrayList<String>(result.keySet());
    		}
    		else if(params[0].type==4)
    		{
    			Map<String,Integer> result=dataProvider.categoryToBooks(Integer.parseInt(params[0].search));
    			data = new ArrayList<String>(result.keySet());
    		}
    		else
    		{
    			Map<String,Integer> result=dataProvider.getAll();
    			data = new ArrayList<String>(result.keySet());
    		}
    		
    		return new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.list_view_layout, data);
		}
		
        protected void onPostExecute(ArrayAdapter<String> result) {
        	if(result.getCount()==0)
        	{
        		Toast alertConn =  Toast.makeText(getActivity(), "Brak wyników - problem z połączeniem?", Toast.LENGTH_LONG);
        		alertConn.show();
        	}
        	else
        	{
        		listView.setAdapter(result);
            	loadingLayout.setVisibility(View.GONE);
            	searchLayout.setVisibility(View.VISIBLE);
        	}
        }
    }
    
    private class UpdateListViewTaskParams
    {
    	public Integer type;
    	public String search;
    }
}
