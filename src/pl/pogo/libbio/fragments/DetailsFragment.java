package pl.pogo.libbio.fragments;

import pl.pogo.libbio.R;
import pl.pogo.libbio.entity.BookDetails;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class DetailsFragment extends DialogFragment {
	View rootView;
	BookDetails details;
	
	TextView title;
	TextView author;
	TextView publisher;
	TextView isbn;
	TextView count;
	TextView avail;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    // Get the layout inflater
	    LayoutInflater inflater = getActivity().getLayoutInflater();

	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    
	    rootView = inflater.inflate(R.layout.details_layout, null);
	    
	    title = (TextView) rootView.findViewById(R.id.detailsTitle);
	    author = (TextView) rootView.findViewById(R.id.detailsAuthor);
	    
	    publisher = (TextView) rootView.findViewById(R.id.detailsPublisher);
	    isbn = (TextView) rootView.findViewById(R.id.detailsIsbn);
	    
	    count = (TextView) rootView.findViewById(R.id.details_COUNT);
	    avail = (TextView) rootView.findViewById(R.id.details_AVAIL);
	    
	    if(details!=null)
	    {
			title.setText(details.title);
			author.setText(details.author);
			publisher.setText(details.publisher);
			isbn.setText(details.ISBN);
			count.setText(details.count.toString());
			avail.setText(details.avail.toString());
	    }
	    
	    builder.setView(rootView);
	    // Add action buttons
	    builder.setPositiveButton("Zamknij", new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   dialog.dismiss();
	               }
	           });    
	    return builder.create();
	}
	public void setDetails(BookDetails details) {
		this.details=details;
	}
	
	
}
